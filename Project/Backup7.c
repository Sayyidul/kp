//backup tanpa interupt usart
// ping pong
unsigned char Read_Buffer[16] absolute 0x500;
unsigned char Write_Buffer[16]absolute 0x510;
unsigned char writebuff[64] = " Bismillah\r\n";
unsigned char writebufflagi[64];
unsigned int data_counter = 0;
unsigned int data_counter1=0;
int counter = 0;
int tanda = 0;
void interrupt() {
        USB_Interrupt_Proc();
        TMR0L = 100;       //Reload Value
        INTCON.TMR0IF = 0;    //Re-Enable Timer-0 Interrupt
}
//LCD 8-bit Mode Connection
sbit LCD_RS at RC1_bit;
sbit LCD_RW at RC0_bit;
sbit LCD_EN at RC2_bit;
sbit LCD_D7 at RD7_bit;
sbit LCD_D6 at RD6_bit;
sbit LCD_D5 at RD5_bit;
sbit LCD_D4 at RD4_bit;
sbit LCD_D3 at RD3_bit;
sbit LCD_D2 at RD2_bit;
sbit LCD_D1 at RD1_bit;
sbit LCD_D0 at RD0_bit;
sbit LCD_RS_Direction at TRISC1_bit;
sbit LCD_RW_Direction at TRISC0_bit;
sbit LCD_EN_Direction at TRISC2_bit;
sbit LCD_D7_Direction at TRISD7_bit;
sbit LCD_D6_Direction at TRISD6_bit;
sbit LCD_D5_Direction at TRISD5_bit;
sbit LCD_D4_Direction at TRISD4_bit;
sbit LCD_D3_Direction at TRISD3_bit;
sbit LCD_D2_Direction at TRISD2_bit;
sbit LCD_D1_Direction at TRISD1_bit;
sbit LCD_D0_Direction at TRISD0_bit;
// End LCD module connections
char i;               // Loop variable

void UART1_Write_Text_Newline(unsigned char msg[]) {
        UART1_Write_Text(msg);
        UART1_Write(10);
        UART1_Write(13);
}
void clear_buffer(unsigned char buffer[]) {
        unsigned int i = 0;
        while (buffer[i] != '\0') {
                buffer[i] = '\0';
                i++;
        }
}
//
void main() {
		writebuff[0] = 11;
        UART1_Init(9600);
        ADCON1 |= 0x0F;          // Configure AN pins as digital
        CMCON |= 7;            // Disable comparators
        TRISB = 0x00;
        TRISC = 0x80;
        Lcd_Init();            // Initialize LCD
        Lcd_Cmd(_LCD_CLEAR);       // Clear display
        Lcd_Cmd(_LCD_CURSOR_OFF);     // Cursor off
        Lcd_Out(1, 1, "TUGAS");        // Write text in first row
        Lcd_Out(2, 1, "Kerja Praktek");        // Write text in second row
        INTCON = 0;
        INTCON2 = 0xF5;
        INTCON3 = 0xC0;
        RCON.IPEN = 0;
        PIE1 = 0;
        PIE2 = 0;
        PIR1 = 0;
        PIR2 = 0;
        //
        // Configure TIMER 0 for 3.3ms interrupts. Set prescaler to 256
        // and load TMR0L to 100 so that the time interval for timer
        // interrupts at 48MHz is 256.(256-100).0.083 = 3.3ms
        //
        // The timer is in 8-bit mode by default
        T0CON = 0x47; // Prescaler = 256
        TMR0L = 100; // Timer count is 256-156 = 100
        INTCON.TMR0IE = 1; // Enable T0IE
        T0CON.TMR0ON = 1; // Turn Timer 0 ON
        INTCON = 0xE0; // Enable interrupts
//
// Enable USB port
//
        Hid_Enable(&Read_Buffer, &Write_Buffer);

        // Read from the USB port. Number of bytes read is in num
start:
        while (Hid_Read() == 0);  //Stay Here if Data is Not Coming from Serial Port

//If Some Data is Coming then move forward and check whether the keyword start is coming or not
        if (Read_Buffer[0] == 0x81) {
                LATD4_bit = 0;
                LATD5_bit = 0;
				for(counter=0;counter<12;counter++){
					Write_Buffer[counter] = writebuff[counter];
				}
				
                while (!HID_Write(&Write_Buffer, 64));
                clear_buffer(Write_Buffer);
                goto loop_second;

        }
        else {
                Lcd_Cmd(_LCD_CLEAR);
                Lcd_Out(1, 2, "Authentication");
                Lcd_Out(2, 5, "Fails!");
                goto start;
        }


loop_second:
        clear_buffer(writebuff);
        clear_buffer(Read_Buffer);
        while (1) {
			if (UART_Data_Ready()) {
				if(tanda==0){
					writebuff[data_counter+1] = UART_Read();
					data_counter++;
					if (data_counter > 62) {
						data_counter1 = 0;
						tanda = 1;
					}
				}else if(tanda==1){
					writebufflagi[data_counter1+1] = UART_Read();
					data_counter1++;
					if (data_counter1 > 62) {
						data_counter = 0;
						tanda = 0;
					}
				}
						
			}
                if (Hid_Read()) {
					if(tanda == 1){
						writebuff[0] = (unsigned char) data_counter;						
						while (!HID_Write(&writebuff, 64)); 
					}else if(tanda == 0){
						writebufflagi[0] = (unsigned char) data_counter1;
						while (!HID_Write(&writebufflagi, 64));
					}					
                       
                }
        }
        data_counter = 0;
        goto loop_second;
}