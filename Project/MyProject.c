
     int data_counter;
     char buffer[16];
     
char USART_ReceiveChar()
{

    while(RCIF==0);                 /*wait for receive interrupt flag*/
    /*if(OERR)
    {
        CREN_bit = 0;
        Nop();
        CREN_bit=1;
    }*/
    return(RCREG);                  /*received in RCREG register and return to main program */
}
     
      //LCD 8-bit Mode Connection
 sbit LCD_RS at RC1_bit;
 sbit LCD_RW at RC0_bit;
 sbit LCD_EN at RC2_bit;
 sbit LCD_D7 at RD7_bit;
 sbit LCD_D6 at RD6_bit;
 sbit LCD_D5 at RD5_bit;
 sbit LCD_D4 at RD4_bit;
 sbit LCD_D3 at RD3_bit;
 sbit LCD_D2 at RD2_bit;
 sbit LCD_D1 at RD1_bit;
 sbit LCD_D0 at RD0_bit;
 sbit LCD_RS_Direction at TRISC1_bit;
 sbit LCD_RW_Direction at TRISC0_bit;
 sbit LCD_EN_Direction at TRISC2_bit;
 sbit LCD_D7_Direction at TRISD7_bit;
 sbit LCD_D6_Direction at TRISD6_bit;
 sbit LCD_D5_Direction at TRISD5_bit;
 sbit LCD_D4_Direction at TRISD4_bit;
 sbit LCD_D3_Direction at TRISD3_bit;
 sbit LCD_D2_Direction at TRISD2_bit;
 sbit LCD_D1_Direction at TRISD1_bit;
 sbit LCD_D0_Direction at TRISD0_bit;
 // End LCD module connections

void main() {
     UART1_Init(9600);
      Delay_ms(100);
      ADCON1 |= 0x0F;          // Configure AN pins as digital
      CMCON |= 7;            // Disable comparators
      TRISB = 0x00;
      TRISC = 0x80;
      Lcd_Init();            // Initialize LCD
      Delay_ms(100);
      Lcd_Cmd(_LCD_CLEAR);       // Clear display
      Delay_ms(100);
      Lcd_Cmd(_LCD_CURSOR_OFF);     // Cursor off
      Delay_ms(100);
      Lcd_Out(1,1,"TUGAS");        // Write text in first row
      Delay_ms(100);
      Lcd_Out(2,1,"Kerja Praktek");        // Write text in second row
      //Delay_ms(2000);


     
     while(1){

     data_counter = 0;

     while(UART_Data_Ready()){
      buffer[0] = USART_ReceiveChar();
     }
     LATB = buffer[0];

     }
     

}