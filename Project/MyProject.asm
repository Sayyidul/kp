
_USART_ReceiveChar:

;MyProject.c,5 :: 		char USART_ReceiveChar()
;MyProject.c,8 :: 		while(RCIF==0);                 /*wait for receive interrupt flag*/
L_USART_ReceiveChar1:
;MyProject.c,15 :: 		return(RCREG);                  /*received in RCREG register and return to main program */
	MOVF        RCREG+0, 0 
	MOVWF       R0 
;MyProject.c,16 :: 		}
L_end_USART_ReceiveChar:
	RETURN      0
; end of _USART_ReceiveChar

_main:

;MyProject.c,43 :: 		void main() {
;MyProject.c,44 :: 		UART1_Init(9600);
	BSF         BAUDCON+0, 3, 0
	MOVLW       4
	MOVWF       SPBRGH+0 
	MOVLW       225
	MOVWF       SPBRG+0 
	BSF         TXSTA+0, 2, 0
	CALL        _UART1_Init+0, 0
;MyProject.c,45 :: 		Delay_ms(100);
	MOVLW       7
	MOVWF       R11, 0
	MOVLW       23
	MOVWF       R12, 0
	MOVLW       106
	MOVWF       R13, 0
L_main2:
	DECFSZ      R13, 1, 1
	BRA         L_main2
	DECFSZ      R12, 1, 1
	BRA         L_main2
	DECFSZ      R11, 1, 1
	BRA         L_main2
	NOP
;MyProject.c,46 :: 		ADCON1 |= 0x0F;          // Configure AN pins as digital
	MOVLW       15
	IORWF       ADCON1+0, 1 
;MyProject.c,47 :: 		CMCON |= 7;            // Disable comparators
	MOVLW       7
	IORWF       CMCON+0, 1 
;MyProject.c,48 :: 		TRISB = 0x00;
	CLRF        TRISB+0 
;MyProject.c,49 :: 		TRISC = 0x80;
	MOVLW       128
	MOVWF       TRISC+0 
;MyProject.c,50 :: 		Lcd_Init();            // Initialize LCD
	CALL        _Lcd_Init+0, 0
;MyProject.c,51 :: 		Delay_ms(100);
	MOVLW       7
	MOVWF       R11, 0
	MOVLW       23
	MOVWF       R12, 0
	MOVLW       106
	MOVWF       R13, 0
L_main3:
	DECFSZ      R13, 1, 1
	BRA         L_main3
	DECFSZ      R12, 1, 1
	BRA         L_main3
	DECFSZ      R11, 1, 1
	BRA         L_main3
	NOP
;MyProject.c,52 :: 		Lcd_Cmd(_LCD_CLEAR);       // Clear display
	MOVLW       1
	MOVWF       FARG_Lcd_Cmd_out_char+0 
	CALL        _Lcd_Cmd+0, 0
;MyProject.c,53 :: 		Delay_ms(100);
	MOVLW       7
	MOVWF       R11, 0
	MOVLW       23
	MOVWF       R12, 0
	MOVLW       106
	MOVWF       R13, 0
L_main4:
	DECFSZ      R13, 1, 1
	BRA         L_main4
	DECFSZ      R12, 1, 1
	BRA         L_main4
	DECFSZ      R11, 1, 1
	BRA         L_main4
	NOP
;MyProject.c,54 :: 		Lcd_Cmd(_LCD_CURSOR_OFF);     // Cursor off
	MOVLW       12
	MOVWF       FARG_Lcd_Cmd_out_char+0 
	CALL        _Lcd_Cmd+0, 0
;MyProject.c,55 :: 		Delay_ms(100);
	MOVLW       7
	MOVWF       R11, 0
	MOVLW       23
	MOVWF       R12, 0
	MOVLW       106
	MOVWF       R13, 0
L_main5:
	DECFSZ      R13, 1, 1
	BRA         L_main5
	DECFSZ      R12, 1, 1
	BRA         L_main5
	DECFSZ      R11, 1, 1
	BRA         L_main5
	NOP
;MyProject.c,56 :: 		Lcd_Out(1,1,"TUGAS");        // Write text in first row
	MOVLW       1
	MOVWF       FARG_Lcd_Out_row+0 
	MOVLW       1
	MOVWF       FARG_Lcd_Out_column+0 
	MOVLW       ?lstr1_MyProject+0
	MOVWF       FARG_Lcd_Out_text+0 
	MOVLW       hi_addr(?lstr1_MyProject+0)
	MOVWF       FARG_Lcd_Out_text+1 
	CALL        _Lcd_Out+0, 0
;MyProject.c,57 :: 		Delay_ms(100);
	MOVLW       7
	MOVWF       R11, 0
	MOVLW       23
	MOVWF       R12, 0
	MOVLW       106
	MOVWF       R13, 0
L_main6:
	DECFSZ      R13, 1, 1
	BRA         L_main6
	DECFSZ      R12, 1, 1
	BRA         L_main6
	DECFSZ      R11, 1, 1
	BRA         L_main6
	NOP
;MyProject.c,58 :: 		Lcd_Out(2,1,"Kerja Praktek");        // Write text in second row
	MOVLW       2
	MOVWF       FARG_Lcd_Out_row+0 
	MOVLW       1
	MOVWF       FARG_Lcd_Out_column+0 
	MOVLW       ?lstr2_MyProject+0
	MOVWF       FARG_Lcd_Out_text+0 
	MOVLW       hi_addr(?lstr2_MyProject+0)
	MOVWF       FARG_Lcd_Out_text+1 
	CALL        _Lcd_Out+0, 0
;MyProject.c,63 :: 		while(1){
L_main7:
;MyProject.c,65 :: 		data_counter = 0;
	CLRF        _data_counter+0 
	CLRF        _data_counter+1 
;MyProject.c,67 :: 		while(UART_Data_Ready()){
L_main9:
	CALL        _UART_Data_Ready+0, 0
	MOVF        R0, 1 
	BTFSC       STATUS+0, 2 
	GOTO        L_main10
;MyProject.c,68 :: 		buffer[0] = USART_ReceiveChar();
	CALL        _USART_ReceiveChar+0, 0
	MOVF        R0, 0 
	MOVWF       _buffer+0 
;MyProject.c,69 :: 		}
	GOTO        L_main9
L_main10:
;MyProject.c,70 :: 		LATB = buffer[0];
	MOVF        _buffer+0, 0 
	MOVWF       LATB+0 
;MyProject.c,72 :: 		}
	GOTO        L_main7
;MyProject.c,75 :: 		}
L_end_main:
	GOTO        $+0
; end of _main
