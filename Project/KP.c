//backup tanpa interupt usart
// ping pong
unsigned char Read_Buffer[64] absolute 0x500;
unsigned char Write_Buffer[64] = " Mengirim Data\r\n" absolute 0x545; // nerima dari seral
unsigned char writebuff[64] = " Menerima Data\r\n" absolute 0x5EE; // dari PC mau ke UART
unsigned char writebufflagi[64] = " Belum ada data\r\n" absolute 0x5AE; //PING-PONG
unsigned int data_counter = 0;
unsigned int data_counter1=0;
int counter = 0;
int tanda = 0;
int terima = 0;
int putar=0;
void interrupt() {
        USB_Interrupt_Proc();
        TMR0L = 100;       //Reload Value
        INTCON.TMR0IF = 0;    //Re-Enable Timer-0 Interrupt
}
char i;               // Loop variable

void UART1_Write_Text_Newline(unsigned char msg[]) {
        UART1_Write_Text(msg);
        UART1_Write(10);
        UART1_Write(13);
}
void clear_buffer(unsigned char buffer[]) {
        unsigned int i = 0;
        while (buffer[i] != '\0') {
                buffer[i] = '\0';
                i++;
        }
}
//
void main() {
                writebuff[0] = 15;
                Write_Buffer[0] = 15;
                writebufflagi[0] = 16;
        UART1_Init(9600);
        ADCON1 |= 0x0F;          // Configure AN pins as digital
        CMCON |= 7;            // Disable comparators
        TRISB = 0b00000000;
        TRISC = 0x80;
        TRISD = 0b00000000; //input 1,output 0
                LATD5_bit = 0;             //RE
        LATD4_bit = 1;                 //DE
        //LATB3_bit = 0; //RC0
        INTCON = 0;
        INTCON2 = 0xF5;
        INTCON3 = 0xC0;
        RCON.IPEN = 0;
        PIE1 = 0;
        PIE2 = 0;
        PIR1 = 0;
        PIR2 = 0;
        //
        // Configure TIMER 0 for 3.3ms interrupts. Set prescaler to 256
        // and load TMR0L to 100 so that the time interval for timer
        // interrupts at 48MHz is 256.(256-100).0.083 = 3.3ms
        //
        // The timer is in 8-bit mode by default
        T0CON = 0x47; // Prescaler = 256
        TMR0L = 100; // Timer count is 256-156 = 100
        INTCON.TMR0IE = 1; // Enable T0IE
        T0CON.TMR0ON = 1; // Turn Timer 0 ON
        INTCON = 0xE0; // Enable interrupts
                
        Hid_Enable(&Read_Buffer, &Write_Buffer);
        
start:
                
loop_second:
        Write_Buffer[0] = 0;
                Read_Buffer[0] = 0;
                writebuff[0] = 0;
                writebufflagi[0] = 0;

        while (1) {
                        if (UART_Data_Ready()) {
                                if(tanda==0){
                                        Write_Buffer[data_counter+1] = UART_Read();
                                        data_counter++;
                                        if (data_counter > 62) {
                                                data_counter1 = 0;
                                                tanda = 1;
                                        }

                                }else if(tanda==1){
                                        writebufflagi[data_counter1+1] = UART_Read();
                                        data_counter1++;
                                        if (data_counter1 > 62) {
                                                data_counter = 0;
                                                tanda = 0;
                                        }

                                }
                                                
                        }
                                                
                                                if (Hid_Read()) {
                                                        switch(Read_Buffer[0]){
                                                                case 0x81:
                                                                if(tanda == 1){
                                                                                Write_Buffer[0] = (unsigned char) data_counter;
                                                                                data_counter=0;
                                                                                tanda = 0;
                                                                                while (!HID_Write(&Write_Buffer, 64));
                                }else if(tanda == 0){
                                                                                writebufflagi[0] = (unsigned char) data_counter1;
                                                                                data_counter1=0;
                                                                                tanda = 1;
                                                                                while (!HID_Write(&writebufflagi, 64));
                                }
                                                                break;
                                                                
                                                                case 0x82:
                                                                writebuff[0] = 10;
                                                                while(!HID_Write(&writebuff,64));
                                                                clear_buffer(writebuff);
                                                                putar = Read_Buffer[1];
                                                                for(terima=0;terima<putar;terima++){
                                                                        writebuff[terima] = Read_Buffer[terima+2];
                                                                }
                                                                
                                                                UART1_Write_Text(writebuff);
                                                                break;
                                                        }    
                                                        
                                                }
        }
        data_counter = 0;
        goto loop_second;
                
}