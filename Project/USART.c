// *******************************************************
// This code will read in the UART data and store the result
// in an array.
//  written in mE C; Default priject settings
//  Tested with EP4 Board and 16F877A 8Mhz
//
//  and a USB-serial adapter.
//  Use at your own risk!!!!
// ********************************************************

const short n = 20;
char rxchar, i = 0, flag = 0; // Variable for storing the data from UART and array counter
unsigned char rxarray[n];   // array to store the received charaters

void interrupt() {
	if (PIR1.RCIF) {          // test the interrupt for uart rx
		rxchar = Usart_Read();  //
		rxarray[i] = rxchar;
		i++;
		// ******************************************************
		// Only select one of the following statements
		if (rxchar == 36) {  // select this if looking for a terminating character
		//if (i == n) (         // select this if looking for a number of characters
			flag = 1;
		} // end if (rxchar == "$")
	} // end  if (PIR1.RCIF)
} // end interrupt


void main() {
	unsigned short j;

	INTCON.GIE = 1;
	INTCON.PEIE = 1;
	PIE1.RCIE = 1; //enable interrupt.
	Usart_Init(9600);

	while (1) {   // Begin endless loop
		// This section is where you tell the program what to do with the data once it
		// has all arrived
		if (flag == 1) {
			j = 0;
			while (j < i) {
				Usart_Write(rxarray[j]);
				j++;
			} // end while(rxarray[i])
			i = 0;
			flag = 0;
		} // end if (flag)
		//************************
		// You can place other things to do in your program here
		// i.e. ADC_readings, turn on outputs, etc.
		// ***********************

	} // end while
} // end main