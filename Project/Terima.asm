
_main:

;Terima.c,4 :: 		void main() {
;Terima.c,5 :: 		UART1_Init(9600);
	BSF         BAUDCON+0, 3, 0
	MOVLW       2
	MOVWF       SPBRGH+0 
	MOVLW       8
	MOVWF       SPBRG+0 
	BSF         TXSTA+0, 2, 0
	CALL        _UART1_Init+0, 0
;Terima.c,6 :: 		ADCON1 |= 0x0F;          // Configure AN pins as digital
	MOVLW       15
	IORWF       ADCON1+0, 1 
;Terima.c,7 :: 		CMCON |= 7;            // Disable comparators
	MOVLW       7
	IORWF       CMCON+0, 1 
;Terima.c,8 :: 		TRISB = 0b00000000;
	CLRF        TRISB+0 
;Terima.c,9 :: 		TRISC = 0b10000000;
	MOVLW       128
	MOVWF       TRISC+0 
;Terima.c,10 :: 		TRISD = 0b00000000; //input 1,output 0
	CLRF        TRISD+0 
;Terima.c,12 :: 		while(1){
L_main0:
;Terima.c,13 :: 		while(UART_Data_Ready()){
L_main2:
	CALL        _UART_Data_Ready+0, 0
	MOVF        R0, 1 
	BTFSC       STATUS+0, 2 
	GOTO        L_main3
;Terima.c,15 :: 		LATB5_bit = 1;
	BSF         LATB5_bit+0, BitPos(LATB5_bit+0) 
;Terima.c,16 :: 		LATB6_bit = 1;
	BSF         LATB6_bit+0, BitPos(LATB6_bit+0) 
;Terima.c,17 :: 		}
	GOTO        L_main2
L_main3:
;Terima.c,19 :: 		terima = 0;
	CLRF        _terima+0 
	CLRF        _terima+1 
;Terima.c,21 :: 		if(rx_buffer[0] == 0x41){
	MOVF        _rx_buffer+0, 0 
	XORLW       65
	BTFSS       STATUS+0, 2 
	GOTO        L_main4
;Terima.c,22 :: 		switch(rx_buffer[1]){
	GOTO        L_main5
;Terima.c,23 :: 		case 0x42:
L_main7:
;Terima.c,24 :: 		LATB5_bit = 1;
	BSF         LATB5_bit+0, BitPos(LATB5_bit+0) 
;Terima.c,25 :: 		LATB6_bit = 0;
	BCF         LATB6_bit+0, BitPos(LATB6_bit+0) 
;Terima.c,26 :: 		break;
	GOTO        L_main6
;Terima.c,27 :: 		case 0x43:
L_main8:
;Terima.c,28 :: 		LATB6_bit = 1;
	BSF         LATB6_bit+0, BitPos(LATB6_bit+0) 
;Terima.c,29 :: 		LATB5_bit = 0;
	BCF         LATB5_bit+0, BitPos(LATB5_bit+0) 
;Terima.c,30 :: 		break;
	GOTO        L_main6
;Terima.c,31 :: 		}
L_main5:
	MOVF        _rx_buffer+1, 0 
	XORLW       66
	BTFSC       STATUS+0, 2 
	GOTO        L_main7
	MOVF        _rx_buffer+1, 0 
	XORLW       67
	BTFSC       STATUS+0, 2 
	GOTO        L_main8
L_main6:
;Terima.c,32 :: 		}
L_main4:
;Terima.c,37 :: 		}
	GOTO        L_main0
;Terima.c,38 :: 		}
L_end_main:
	GOTO        $+0
; end of _main
