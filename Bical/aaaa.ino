#include<Servo.h>

Servo channel1;
Servo channel2;
Servo channel3;
Servo channel4;
Servo channel5;
Servo channel6;
Servo channel7;
Servo channel8;

unsigned long counter_1,current_count;
byte last_CH1_state;
int Ch1;
int O[9];
int pulsa[9];
int i=0;
int a=0;
void setup() {

  
  PCICR |= 0b00000100;    //enable PCIE2 scan                                                 
  PCMSK2 |= (1 << PCINT20);  //Set pin A12 trigger an interrupt on state change.  
                                                
  Serial.begin(9600);  
  channel1.attach(13);
  channel2.attach(12);
  channel3.attach(11);
  channel4.attach(10);
  channel5.attach(9);
  channel6.attach(8);
  channel7.attach(6);
  
  channel8.attach(5);


}

void loop() {
 for(a=1;a<9;a++){
    O[a] = map(pulsa[a], 1000, 2000, 0, 180);
    Serial.print(pulsa[a]);
    Serial.print("\t");
  }
  Serial.print("\n");
  channel1.write(O[1]);
  channel2.write(O[2]);
  channel3.write(O[3]);
  channel4.write(O[4]);
  channel5.write(O[5]);
  channel6.write(O[6]);
  channel7.write(O[7]);
  channel8.write(O[8]);
 

}




//This is the interruption routine
//----------------------------------------------

ISR(PCINT2_vect){
//First we take the current count value in micro seconds using the micros() function
  
  current_count = micros();
  ///////////////////////////////////////Channel 1
  
  if(PINK & B00010000){                              //A12
    if(last_CH1_state == 0){                         //If the last state was 0, then we have a state change...
      last_CH1_state = 1;                            //Store the current state into the last state for the next loop
      counter_1 = current_count;                     //Set counter_1 to current value.
    } 
  }
  else if(last_CH1_state == 1){                      //If pin 8 is LOW and the last state was HIGH then we have a state change      
    last_CH1_state = 0;                              //Store the current state into the last state for the next loop
    i++;
    Ch1 = current_count - counter_1;                //We make the time difference. Channel 1 is current_time - timer_1.
    if(Ch1 >2000){
      
      pulsa[0] = Ch1;
      i=0;
    }else{
      pulsa[i] = Ch1;
    }
  
  }
  
 
}
