﻿//#define ACTIVE_INITIAL_BAUDRATE_SET

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using usbGenericHidCommunications;
using USB_Generic_HID_reference_application;

namespace KP_v1._0 {
	public partial class Form1 : Form {
		Byte[] data_in = new Byte[65];
		Byte[] data_out = new Byte[65];
		string[] separator = { " , ", " . ", " ", "\t", "\n" };
		uint[] baudrate = {1200, 2400, 4800, 9600, 19200, 38400, 57600, 115200};
		int separator_index = 0;
		int byte_counter = 0;
		bool USB_status = false;


		//usb reference device instance
		private usbReferenceDevice theReferenceUsbDevice;

		public Form1() {
			InitializeComponent();
			// Create the USB reference device object (passing VID and PID)
			theReferenceUsbDevice = new usbReferenceDevice(0x04D8, 0x0042);

			// Add a listener for usb events
			theReferenceUsbDevice.usbEvent += new usbReferenceDevice.usbEventsHandler(usbEvent_receiver);

			// Perform an initial search for the target device
			theReferenceUsbDevice.findTargetDevice();

			// load combo box initial index
			this.separator_comboBox.SelectedIndex = 0;
			this.baudRate_comboBox.SelectedIndex = 3;

			// reload byte counter display
			this.byteReceived_lineEdit.Text = byte_counter.ToString();

			// display initial debug text
			this.debug_textBox.Text = "RS485 sniffer V1.0.0 \nProject KP juli - agustus 2018 \nInstitut Teknologi Sepuluh Nopember \n";
			if(theReferenceUsbDevice.isDeviceAttached)
				this.debug_textBox.AppendText("Device Connected \n");

			if(theReferenceUsbDevice.isDeviceAttached && USB_status) {
			#if ACTIVE_INITIAL_BAUDRATE_SET
				if(theReferenceUsbDevice.isDeviceAttached) {

					this.debug_textBox.AppendText("Setting baud rate to -> " + baudrate[this.baudRate_comboBox.SelectedIndex] + "\n");
					for(int i = 0; i < 10; i++) {
						this.debug_textBox.AppendText("trial..." + (i + 1) + " \n");

						if(this.theReferenceUsbDevice.isDeviceAttached && this.theReferenceUsbDevice.request_bdrate_change(ref data_in, ref data_out, baudrate[this.baudRate_comboBox.SelectedIndex])) {
							this.debug_textBox.AppendText("Baud rate set to -> " + baudrate[this.baudRate_comboBox.SelectedIndex] + "\n");
							break;
						}
						else if(!this.theReferenceUsbDevice.isDeviceAttached) {
							this.debug_textBox.AppendText("Failed device disconnected \n");
							break;
						}

						if(i==9)
							this.debug_textBox.AppendText("failed, running on default baud rate \n");
					}
				}
			#endif
			}

		}

		private void Form1_Load(object sender, EventArgs e) {
		}

		//USB event callback
		private void usbEvent_receiver(object o, EventArgs e) {
			if(theReferenceUsbDevice.isDeviceAttached && !USB_status) {
				this.debug_textBox.AppendText("Device connected \n");
				USB_status = true;
			}

			else if (!theReferenceUsbDevice.isDeviceAttached && USB_status) {
				this.debug_textBox.AppendText("Device disconnected \n");
				USB_status = false;
			}

			// set initial baudrate
			else if(theReferenceUsbDevice.isDeviceAttached && USB_status) {
			#if ACTIVE_INITIAL_BAUDRATE_SET
				if(theReferenceUsbDevice.isDeviceAttached) {

					this.debug_textBox.AppendText("Setting baud rate to -> " + baudrate[this.baudRate_comboBox.SelectedIndex] + "\n");
					for(int i = 0; i < 10; i++) {
						this.debug_textBox.AppendText("trial..." + (i + 1) + " \n");

						if(this.theReferenceUsbDevice.isDeviceAttached && this.theReferenceUsbDevice.request_bdrate_change(ref data_in, ref data_out, baudrate[this.baudRate_comboBox.SelectedIndex])) {
							this.debug_textBox.AppendText("Baud rate set to -> " + baudrate[this.baudRate_comboBox.SelectedIndex] + "\n");
							break;
						}
						else if(!this.theReferenceUsbDevice.isDeviceAttached) {
							this.debug_textBox.AppendText("Failed device disconnected \n");
							break;
						}

						if(i == 9)
							this.debug_textBox.AppendText("failed, running on default baud rate \n");
					}
				}
			#endif
			}

			//initial baudrate request
		}

		//Polling timer callback
		private void dataPolling_timer_Tick(object sender, EventArgs e) {
			string HEXstring, ASCIIstring;
			int data_count = 0;

			if(theReferenceUsbDevice.isDeviceAttached && USB_status) {
				data_count = theReferenceUsbDevice.single_packet_poll_v2(ref data_in);
				if(data_count > 0) {
					byte_counter += data_count;
					this.byteReceived_lineEdit.Text = byte_counter.ToString();

					//convert array of byte to ASCII string
					for(int i = 0; i < data_count; i++) {
						HEXstring = BitConverter.ToString(data_in, i + 2, 1);
						this.HEX_textEdit.AppendText(HEXstring);
						this.HEX_textEdit.AppendText(separator[separator_index]);
					}

					//convert array of byte to ASCII string
					ASCIIstring = System.Text.Encoding.ASCII.GetString(data_in, 2, data_count);
					this.ASCII_textEdit.AppendText(ASCIIstring);

				}
				debug_textBox.AppendText(data_count + "\n");
			}
		}

		//Scroll button
		private void scrollToEnd_button_Click(object sender, EventArgs e) {
			this.HEX_textEdit.SelectionStart = this.HEX_textEdit.Text.Length;
			this.HEX_textEdit.ScrollToCaret();
			this.ASCII_textEdit.SelectionStart = this.ASCII_textEdit.Text.Length;
			this.ASCII_textEdit.ScrollToCaret();
		}

		//Autoscroll
		private void debug_textBox_TextChanged(object sender, EventArgs e) {
			this.debug_textBox.SelectionStart = this.debug_textBox.Text.Length;
			this.debug_textBox.ScrollToCaret();
		}

		private void HEX_textEdit_TextChanged(object sender, EventArgs e) {
			if(autoScroll_checkBox.Checked) {
				this.HEX_textEdit.SelectionStart = this.HEX_textEdit.Text.Length;
				this.HEX_textEdit.ScrollToCaret();
			}
		}

		private void ASCII_textEdit_TextChanged(object sender, EventArgs e) {
			if(autoScroll_checkBox.Checked) {
				this.ASCII_textEdit.SelectionStart = this.ASCII_textEdit.Text.Length;
				this.ASCII_textEdit.ScrollToCaret();
			}
		}

		private void ASCII_textEdit_MouseUp(object sender, MouseEventArgs e) {
			this.ASCII_textEdit.Enabled = false;
			this.ASCII_textEdit.Enabled = true;
		}

		private void HEX_textEdit_MouseUp_1(object sender, MouseEventArgs e) {
			this.HEX_textEdit.Enabled = false;
			this.HEX_textEdit.Enabled = true;
		}

		//separator character changed
		private void separator_comboBox_DropDownClosed(object sender, EventArgs e) {
			separator_index = separator_comboBox.SelectedIndex;
		}

		//clear display
		private void Clear_button_Click(object sender, EventArgs e) {
			this.HEX_textEdit.Clear();
			this.ASCII_textEdit.Clear();
			this.debug_textBox.AppendText("Display Cleared\n");
		}

		//reset data counter
		private void resetCounter_button_Click(object sender, EventArgs e) {
			this.debug_textBox.AppendText("Counter reset (" + byte_counter.ToString()  + ") \n");
			byte_counter = 0;
			this.byteReceived_lineEdit.Text = byte_counter.ToString();
		}

		//baudrate changed
		private void baudRate_comboBox_DropDownClosed(object sender, EventArgs e) {

		}
	}

}
