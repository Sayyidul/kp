﻿namespace KP_v1._0
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
			this.components = new System.ComponentModel.Container();
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
			this.HEX_textEdit = new System.Windows.Forms.RichTextBox();
			this.ASCII_textEdit = new System.Windows.Forms.RichTextBox();
			this.label2 = new System.Windows.Forms.Label();
			this.label1 = new System.Windows.Forms.Label();
			this.resetCounter_button = new System.Windows.Forms.Button();
			this.byteReceived_lineEdit = new System.Windows.Forms.TextBox();
			this.debug_textBox = new System.Windows.Forms.RichTextBox();
			this.dataPolling_timer = new System.Windows.Forms.Timer(this.components);
			this.groupBox1 = new System.Windows.Forms.GroupBox();
			this.groupBox2 = new System.Windows.Forms.GroupBox();
			this.autoScroll_checkBox = new System.Windows.Forms.CheckBox();
			this.groupBox3 = new System.Windows.Forms.GroupBox();
			this.groupBox5 = new System.Windows.Forms.GroupBox();
			this.baudRate_comboBox = new System.Windows.Forms.ComboBox();
			this.Clear_button = new System.Windows.Forms.Button();
			this.scrollToEnd_button = new System.Windows.Forms.Button();
			this.groupBox4 = new System.Windows.Forms.GroupBox();
			this.separator_comboBox = new System.Windows.Forms.ComboBox();
			this.logoITS_pictureBox = new System.Windows.Forms.PictureBox();
			this.groupBox1.SuspendLayout();
			this.groupBox2.SuspendLayout();
			this.groupBox3.SuspendLayout();
			this.groupBox5.SuspendLayout();
			this.groupBox4.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.logoITS_pictureBox)).BeginInit();
			this.SuspendLayout();
			// 
			// HEX_textEdit
			// 
			this.HEX_textEdit.BorderStyle = System.Windows.Forms.BorderStyle.None;
			this.HEX_textEdit.Cursor = System.Windows.Forms.Cursors.No;
			this.HEX_textEdit.DetectUrls = false;
			this.HEX_textEdit.Font = new System.Drawing.Font("Century", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.HEX_textEdit.Location = new System.Drawing.Point(12, 35);
			this.HEX_textEdit.Name = "HEX_textEdit";
			this.HEX_textEdit.ReadOnly = true;
			this.HEX_textEdit.ScrollBars = System.Windows.Forms.RichTextBoxScrollBars.Vertical;
			this.HEX_textEdit.Size = new System.Drawing.Size(405, 213);
			this.HEX_textEdit.TabIndex = 12;
			this.HEX_textEdit.Text = "";
			this.HEX_textEdit.TextChanged += new System.EventHandler(this.HEX_textEdit_TextChanged);
			this.HEX_textEdit.MouseUp += new System.Windows.Forms.MouseEventHandler(this.HEX_textEdit_MouseUp_1);
			// 
			// ASCII_textEdit
			// 
			this.ASCII_textEdit.BorderStyle = System.Windows.Forms.BorderStyle.None;
			this.ASCII_textEdit.Cursor = System.Windows.Forms.Cursors.IBeam;
			this.ASCII_textEdit.Font = new System.Drawing.Font("Century", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.ASCII_textEdit.Location = new System.Drawing.Point(433, 35);
			this.ASCII_textEdit.Name = "ASCII_textEdit";
			this.ASCII_textEdit.ReadOnly = true;
			this.ASCII_textEdit.ScrollBars = System.Windows.Forms.RichTextBoxScrollBars.Vertical;
			this.ASCII_textEdit.Size = new System.Drawing.Size(405, 213);
			this.ASCII_textEdit.TabIndex = 14;
			this.ASCII_textEdit.Text = "";
			this.ASCII_textEdit.TextChanged += new System.EventHandler(this.ASCII_textEdit_TextChanged);
			this.ASCII_textEdit.MouseUp += new System.Windows.Forms.MouseEventHandler(this.ASCII_textEdit_MouseUp);
			// 
			// label2
			// 
			this.label2.AutoSize = true;
			this.label2.Location = new System.Drawing.Point(430, 15);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(41, 17);
			this.label2.TabIndex = 4;
			this.label2.Text = "ASCII";
			// 
			// label1
			// 
			this.label1.AutoSize = true;
			this.label1.Location = new System.Drawing.Point(12, 15);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(36, 17);
			this.label1.TabIndex = 6;
			this.label1.Text = "HEX";
			// 
			// resetCounter_button
			// 
			this.resetCounter_button.Location = new System.Drawing.Point(138, 21);
			this.resetCounter_button.Name = "resetCounter_button";
			this.resetCounter_button.Size = new System.Drawing.Size(129, 36);
			this.resetCounter_button.TabIndex = 17;
			this.resetCounter_button.Text = "Reset counter";
			this.resetCounter_button.UseVisualStyleBackColor = true;
			this.resetCounter_button.Click += new System.EventHandler(this.resetCounter_button_Click);
			// 
			// byteReceived_lineEdit
			// 
			this.byteReceived_lineEdit.Enabled = false;
			this.byteReceived_lineEdit.Font = new System.Drawing.Font("Swis721 BlkCn BT", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.byteReceived_lineEdit.Location = new System.Drawing.Point(6, 21);
			this.byteReceived_lineEdit.Name = "byteReceived_lineEdit";
			this.byteReceived_lineEdit.ReadOnly = true;
			this.byteReceived_lineEdit.Size = new System.Drawing.Size(126, 35);
			this.byteReceived_lineEdit.TabIndex = 8;
			// 
			// debug_textBox
			// 
			this.debug_textBox.BackColor = System.Drawing.SystemColors.Info;
			this.debug_textBox.BorderStyle = System.Windows.Forms.BorderStyle.None;
			this.debug_textBox.Location = new System.Drawing.Point(6, 21);
			this.debug_textBox.Name = "debug_textBox";
			this.debug_textBox.ReadOnly = true;
			this.debug_textBox.ScrollBars = System.Windows.Forms.RichTextBoxScrollBars.ForcedVertical;
			this.debug_textBox.ShortcutsEnabled = false;
			this.debug_textBox.Size = new System.Drawing.Size(365, 208);
			this.debug_textBox.TabIndex = 10;
			this.debug_textBox.Text = "";
			this.debug_textBox.TextChanged += new System.EventHandler(this.debug_textBox_TextChanged);
			// 
			// dataPolling_timer
			// 
			this.dataPolling_timer.Enabled = true;
			this.dataPolling_timer.Tick += new System.EventHandler(this.dataPolling_timer_Tick);
			// 
			// groupBox1
			// 
			this.groupBox1.Controls.Add(this.debug_textBox);
			this.groupBox1.Location = new System.Drawing.Point(15, 254);
			this.groupBox1.Name = "groupBox1";
			this.groupBox1.Size = new System.Drawing.Size(377, 236);
			this.groupBox1.TabIndex = 11;
			this.groupBox1.TabStop = false;
			this.groupBox1.Text = "Debug";
			// 
			// groupBox2
			// 
			this.groupBox2.Controls.Add(this.byteReceived_lineEdit);
			this.groupBox2.Controls.Add(this.resetCounter_button);
			this.groupBox2.Location = new System.Drawing.Point(559, 254);
			this.groupBox2.Name = "groupBox2";
			this.groupBox2.Size = new System.Drawing.Size(279, 65);
			this.groupBox2.TabIndex = 12;
			this.groupBox2.TabStop = false;
			this.groupBox2.Text = "Byte received";
			// 
			// autoScroll_checkBox
			// 
			this.autoScroll_checkBox.AutoSize = true;
			this.autoScroll_checkBox.Location = new System.Drawing.Point(6, 89);
			this.autoScroll_checkBox.Name = "autoScroll_checkBox";
			this.autoScroll_checkBox.Size = new System.Drawing.Size(96, 21);
			this.autoScroll_checkBox.TabIndex = 17;
			this.autoScroll_checkBox.Text = "Auto scroll";
			this.autoScroll_checkBox.UseVisualStyleBackColor = true;
			// 
			// groupBox3
			// 
			this.groupBox3.Controls.Add(this.groupBox5);
			this.groupBox3.Controls.Add(this.Clear_button);
			this.groupBox3.Controls.Add(this.scrollToEnd_button);
			this.groupBox3.Controls.Add(this.autoScroll_checkBox);
			this.groupBox3.Controls.Add(this.groupBox4);
			this.groupBox3.Location = new System.Drawing.Point(398, 254);
			this.groupBox3.Name = "groupBox3";
			this.groupBox3.Size = new System.Drawing.Size(155, 236);
			this.groupBox3.TabIndex = 14;
			this.groupBox3.TabStop = false;
			this.groupBox3.Text = "Control";
			// 
			// groupBox5
			// 
			this.groupBox5.Controls.Add(this.baudRate_comboBox);
			this.groupBox5.Location = new System.Drawing.Point(6, 176);
			this.groupBox5.Name = "groupBox5";
			this.groupBox5.Size = new System.Drawing.Size(143, 53);
			this.groupBox5.TabIndex = 18;
			this.groupBox5.TabStop = false;
			this.groupBox5.Text = "Baud rate";
			// 
			// baudRate_comboBox
			// 
			this.baudRate_comboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.baudRate_comboBox.FormattingEnabled = true;
			this.baudRate_comboBox.Items.AddRange(new object[] {
            "1200",
            "2400",
            "4800",
            "9600",
            "19200",
            "38400",
            "57600",
            "115200"});
			this.baudRate_comboBox.Location = new System.Drawing.Point(6, 21);
			this.baudRate_comboBox.Name = "baudRate_comboBox";
			this.baudRate_comboBox.Size = new System.Drawing.Size(131, 24);
			this.baudRate_comboBox.TabIndex = 16;
			this.baudRate_comboBox.DropDownClosed += new System.EventHandler(this.baudRate_comboBox_DropDownClosed);
			// 
			// Clear_button
			// 
			this.Clear_button.Location = new System.Drawing.Point(6, 21);
			this.Clear_button.Name = "Clear_button";
			this.Clear_button.Size = new System.Drawing.Size(143, 28);
			this.Clear_button.TabIndex = 13;
			this.Clear_button.Text = "Clear";
			this.Clear_button.UseVisualStyleBackColor = true;
			this.Clear_button.Click += new System.EventHandler(this.Clear_button_Click);
			// 
			// scrollToEnd_button
			// 
			this.scrollToEnd_button.Location = new System.Drawing.Point(6, 55);
			this.scrollToEnd_button.Name = "scrollToEnd_button";
			this.scrollToEnd_button.Size = new System.Drawing.Size(143, 28);
			this.scrollToEnd_button.TabIndex = 15;
			this.scrollToEnd_button.Text = "Scroll to end";
			this.scrollToEnd_button.UseVisualStyleBackColor = true;
			this.scrollToEnd_button.Click += new System.EventHandler(this.scrollToEnd_button_Click);
			// 
			// groupBox4
			// 
			this.groupBox4.Controls.Add(this.separator_comboBox);
			this.groupBox4.Location = new System.Drawing.Point(6, 116);
			this.groupBox4.Name = "groupBox4";
			this.groupBox4.Size = new System.Drawing.Size(143, 54);
			this.groupBox4.TabIndex = 17;
			this.groupBox4.TabStop = false;
			this.groupBox4.Text = "Separator";
			// 
			// separator_comboBox
			// 
			this.separator_comboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.separator_comboBox.FormattingEnabled = true;
			this.separator_comboBox.ImeMode = System.Windows.Forms.ImeMode.On;
			this.separator_comboBox.Items.AddRange(new object[] {
            "\",\"",
            "\".\"",
            "Space",
            "TAB",
            "Enter"});
			this.separator_comboBox.Location = new System.Drawing.Point(6, 21);
			this.separator_comboBox.Name = "separator_comboBox";
			this.separator_comboBox.Size = new System.Drawing.Size(131, 24);
			this.separator_comboBox.TabIndex = 16;
			this.separator_comboBox.DropDownClosed += new System.EventHandler(this.separator_comboBox_DropDownClosed);
			// 
			// logoITS_pictureBox
			// 
			this.logoITS_pictureBox.Image = ((System.Drawing.Image)(resources.GetObject("logoITS_pictureBox.Image")));
			this.logoITS_pictureBox.ImageLocation = "";
			this.logoITS_pictureBox.Location = new System.Drawing.Point(565, 325);
			this.logoITS_pictureBox.Name = "logoITS_pictureBox";
			this.logoITS_pictureBox.Size = new System.Drawing.Size(273, 165);
			this.logoITS_pictureBox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
			this.logoITS_pictureBox.TabIndex = 15;
			this.logoITS_pictureBox.TabStop = false;
			// 
			// Form1
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.BackColor = System.Drawing.SystemColors.Window;
			this.ClientSize = new System.Drawing.Size(848, 501);
			this.Controls.Add(this.logoITS_pictureBox);
			this.Controls.Add(this.groupBox3);
			this.Controls.Add(this.groupBox2);
			this.Controls.Add(this.groupBox1);
			this.Controls.Add(this.label1);
			this.Controls.Add(this.label2);
			this.Controls.Add(this.ASCII_textEdit);
			this.Controls.Add(this.HEX_textEdit);
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
			this.MaximizeBox = false;
			this.Name = "Form1";
			this.Text = "RS485 sniffer GUI";
			this.Load += new System.EventHandler(this.Form1_Load);
			this.groupBox1.ResumeLayout(false);
			this.groupBox2.ResumeLayout(false);
			this.groupBox2.PerformLayout();
			this.groupBox3.ResumeLayout(false);
			this.groupBox3.PerformLayout();
			this.groupBox5.ResumeLayout(false);
			this.groupBox4.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.logoITS_pictureBox)).EndInit();
			this.ResumeLayout(false);
			this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.RichTextBox HEX_textEdit;
        private System.Windows.Forms.RichTextBox ASCII_textEdit;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button resetCounter_button;
        private System.Windows.Forms.TextBox byteReceived_lineEdit;
        private System.Windows.Forms.RichTextBox debug_textBox;
        private System.Windows.Forms.Timer dataPolling_timer;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.CheckBox autoScroll_checkBox;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Button Clear_button;
        private System.Windows.Forms.Button scrollToEnd_button;
        private System.Windows.Forms.PictureBox logoITS_pictureBox;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.ComboBox separator_comboBox;
		private System.Windows.Forms.GroupBox groupBox5;
		private System.Windows.Forms.ComboBox baudRate_comboBox;
	}
}

