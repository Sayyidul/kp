/*
 * File:   Transmit_Char.c
 *
 * www.electronicwings.com
 */
#include <pic18f4550.h>
#include "Configuration_Header_File.h"

void USART_Init(long);
void USART_TxChar(char);
void USART_SendString(const char *);
void MSdelay(unsigned int val);

#define F_CPU 8000000/64

//#define Baud_value(baud_rate) (((float)(F_CPU)/(float)baud_rate)-1)

#define Baud_value (((float)(F_CPU)/(float)baud_rate)-1)
void main()
{
    OSCCON=0x72;                        /*set internal oscillator frequency= 8 MHz*/    
    USART_Init(9600);                   /*initialize USART with 9600 baud rate*/   
    TRISD = 0b11111111;
    //USART_TxChar(0xFF);           /*Transmit character '"'  */
//    USART_SendString("A");    /*Transmit String*/
//    //USART_TxChar(0x22);           /*Transmit character '"' */
//    MSdelay(3000);
//    USART_SendString("B");
//    MSdelay(3000);
//    USART_SendString("C");
//    MSdelay(3000);
//    USART_SendString("D");
    
//    for(int a=0;a<26;a++){
//        int b=65;
//        b=b+a;
        //USART_TxChar(0b00000001);
//        MSdelay(3000);
//    }
    
    while(1){
        USART_SendString("Bismillah\r\n");
//        if(PORTDbits.RD2){
//        USART_SendString("AB");
//        }
//        
//        if(PORTDbits.RD0){
//        USART_SendString("AC");
//        }
        
        
    };
    
}

/*****************************USART Initialization*******************************/
void USART_Init(long baud_rate)
{
    float temp;
    TRISC6=0;                       /*Make Tx pin as output*/
    TRISC7=1;                       /*Make Rx pin as input*/
    temp=Baud_value;     
    SPBRG=(int)temp;                /*baud rate=9600, SPBRG = (F_CPU /(64*9600))-1*/
    TXSTA=0x20;                     /*Transmit Enable(TX) enable*/ 
    RCSTA=0x80;                     /*Receive Enable(RX) enable and serial port enable */
}
/******************TRANSMIT FUNCTION*****************************************/ 
void USART_TxChar(char out)
{        
        while(TXIF==0);            /*wait for transmit interrupt flag*/
        TXREG=out;                 /*wait for transmit interrupt flag to set which indicates TXREG is ready
                                    for another transmission*/    
}

void USART_SendString(const char *out)
{
   while(*out!='\0')
   {            
        USART_TxChar(*out);
        out++;
   }
}

void MSdelay(unsigned int val)
{
     unsigned int i,j;
        for(i=0;i<=val;i++)
            for(j=0;j<81;j++);      /*This count Provide delay of 1 ms for 8MHz Frequency */
 }

